// `timescale <time_unit>/<time_precision>
`timescale 10ns/1ns

module test;
    logic a = 0;
    logic b = 0;
    wire out, c;
    
    half_adder hadd(.a(a), .b(b), .out(out), .c(c));

    initial begin
        # 10  a = 0; b = 0;
        # 20  a = 0; b = 1;
        # 30  a = 1; b = 0;
        # 40  a = 1; b = 1;
        # 100 $finish;
    end
    
    initial begin
        $monitor("At time %t, a=%h, b=%h, out = %h, c = %h",
                 $time, a, b, out, c);
    end
              
    initial begin
        $dumpfile("test.vcd"); // the name of the file to write the dumped vars to
        $dumplimit(65536);     // the limit in bytes of the file size
        $dumpvars(0, test);    // which modules' vars to dump
    end
endmodule // test
