module test;

    int a_array[string];
    
    initial begin
        a_array["Hello"] = 1;
        a_array["Bye"] = 2;
        a_array["Hi"] = 3;
        
        foreach (a_array[key])
            $display("a_array[%0s] = %0d", index, a_array[index]);
        
        if (a_array.exists("Hi"))
            $display("\"Hi\" exists in a_array");
        else
            $display("\"Hi\" doesn't exist in a_array");
        
        a_array.delete("Hi");
        
        if (a_array.exists("Hi"))
            $display("\"Hi\" exists in a_array");
        else
            $display("\"Hi\" doesn't exist in a_array");
        
        // delete all items
        a_array.delete();
        
        $finish;
    end
    
endmodule
