@echo off

Rem Put all Verilog source files names into a text file
call ../enum_files.bat "*.sv" > file_list.txt

Rem Build the design
call ../build_and_run_file_list.bat file_list.txt
