class Rational;
    // static properties
    static int nInstances;

    // instance properties
    int numerator;
    int denominator;
    
    // constructor
    // (SystemVerilog doesn't support function overload,
    // so there can be only one constructor)
    function new();
        this.numerator = 0;
        this.denominator = 1;
        nInstances++;
    endfunction
    
    // deep copy
    function Rational copy;
        copy = new();
        copy.numerator = this.numerator;
        copy.denominator = this.denominator;
        return copy;
    endfunction
        
    // method
    task set(int _numerator, int _denominator);
        this.numerator = _numerator;
        this.denominator = _denominator;
    endtask
    
    // property setter
    task setNumerator(int value);
        numerator = value;
    endtask
    
    // property getter
    function int getNumerator();
        return numerator;
    endfunction
        
    // property setter
    task setDenominator(int value);
        denominator = value;
    endtask
        
    // property getter
    function int getDenominator();
        return denominator;
    endfunction

    // method
    task Display();
        $display("%0d / %0d", numerator, denominator);
    endtask
endclass

module test;

    Rational r1;
    Rational r2;
    Rational r3;
    initial begin
        r1 = new();
        r1.set(1, 2);
        
        r2 = new();
        r2.set(2, 3);

        r1.Display();
        r2.Display();
        
        r2 = r1;     // assignment
        r2 = new r1; // memory allocation and shallow copying
        r2.Display();
        
        r3 = r2.copy(); // deep copy
        r3.Display();
        
        $display("%0d", Rational::nInstances);
    end

endmodule
