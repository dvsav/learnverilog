module test;

    function int sum(input int a, b);
        return a + b; // alternatively you can write: sum = a + b;
    endfunction
    
    function int max(int a, int b);
        max = a > b ? a : b; // alternatively you can write: return a > b ? a : b;
    endfunction
    
    function void sayhello();
        $display("Hello!");
    endfunction
    
    // a function by default is static but we can change it to automatic
    function automatic void swap(ref int a, ref int b);
        automatic int tmp = a; // a variable by default is static but we can change it to automatic (local variable)
        a = b;
        b = tmp;
    endfunction
 
    int x;
    int y;
    
    initial begin
        x = sum(10, 5);
        $display("x = %0d", x);
        
        sayhello();
        
        $display("max = %0d", max(10, 5));
        
        x = 3;
        y = 5;
        swap(x, y);
        $display("x = %0d y = %0d", x, y);
    end

endmodule
