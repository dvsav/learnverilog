module test;

    task sum(input int a, b, output int c);
        c = a + b;
        #5 $display("Task sum has finished");
    endtask
 
    int x;
    initial begin
        sum(10, 5, x);
        $display("x = %0d", x);
    end

endmodule
