// `timescale <time_unit>/<time_precision>
`timescale 10ns/1ns

module test;

    bit a;

    always begin
        #1 a = ~a;
    end
    
    always @(posedge a or negedge a) begin
        $display("a = %b", a);
    end
    
    initial begin
        #10 $finish;
    end

endmodule
