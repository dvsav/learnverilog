module test;

int a = 10;

initial
    begin
        
        repeat(a) begin
            $display("Hello!");
        end
        
        forever begin
            $display("a = %0d", a);
            if (a-- < 5)
                $finish;
        end
    end

endmodule
