// `timescale <time_unit>/<time_precision>
`timescale 10ns/1ns

module test;

initial
  begin
    bit[7:0] a, b, c;
    
    a = 10;
    b = 15;
    c = 20;
    
    // conditions are evaluated in parallel
    unique if (a < b)
        $display("a < b");
    else if (b < c)
        $display("b < c");
    else
        $display("neither");
    
    // conditions are evaluated sequentially
    priority if (a < b)
        $display("a < b");
    else if (b < c)
        $display("b < c");
    else
        $display("neither");
    
    $finish;
  end

endmodule
