// virtual class = abstract class (cannot be instantiated)
virtual class IPrintable;
    pure virtual function Display();
endclass
 
class MyClass extends IPrintable;
    bit [31:0] data;
    
    virtual task Display();
        $display("data = %0d", data);
    endtask
endclass

module test;

    MyClass c;
    initial begin
        c = new();
        c.data = 10;
        c.Display();
    end

endmodule
