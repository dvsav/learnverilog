module rand_methods;
    bit [2:0] addr;
    
    int val;
    
    bit [63:0] longlong;
    
    bit [5:0] ranged;
    
    initial begin
        repeat(10) begin
            // $urandom(<seed>) gemerates 32-bit unsigned pseudorandom numbers
            addr = $urandom();
            
            // $random() is same as $urandom() but it generates signed numbers
            val = $random();
            
            // this is how you can generate 64-bit random numbers
            longlong = { $urandom(), $urandom() };
            
            // generates random numbers in a specified range (max, min)
            ranged = $urandom_range(30, 20);
            
            $display("addr = %0d val = %d longlong = %X ranged = %05X",
                     addr, val, longlong, ranged);
        end
    end
endmodule