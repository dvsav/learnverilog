// base class
class base_class;
    virtual function void display();
        $display("Inside base class");
    endfunction
endclass
 
// child class
class child_class extends base_class;
    function void display();
        super.display();
        $display("Inside child class");
    endfunction
endclass

module casting;
    real r_a;
    int  i_a;
     
    initial begin
        r_a = (2.1 * 3.2);
         
        // static cast from real to int
        i_a = int'(2.1 * 3.2); //or i_a = int'(r_a);
         
        $display("real value is %0f", r_a);
        $display("int  value is %0d", i_a);
    end
    
    initial begin
        parent_class p;
        child_class  c=new();
        child_class  c1;
        c.addr = 10;
        c.data = 20;
 
        p = c;         // p is pointing to child class handle c.
        // c = p;      // compilation error!
        $cast(c, p);   // c = dynamic_cast<child_class>(p); type chek will occur during runtime
 
        c1.display();
    end
endmodule