module freq_divider(
    input clk,
    input rst,
    output logic out);

    parameter WIDTH = 8;
    parameter DIVISOR = 2;

    logic [WIDTH-1 : 0] counter;

    always @(edge clk) begin
        if (rst) begin
            counter <= 0;
            out <= 0;
            end
        else begin
            if (counter < DIVISOR)
                counter <= counter + 1;
            else begin
                counter <= 0;
                out <= !out;
                end
            end
    end

endmodule
