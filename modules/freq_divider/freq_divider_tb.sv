// `timescale <time_unit>/<time_precision>
`timescale 10ns/1ns

module test;

  logic clk = 0;
  logic rst = 1;

  wire out;
  freq_divider fdiv(.clk(clk), .rst(rst), .out(out));
  
  always #5 clk = !clk;
  
  initial begin
      $monitor("At time %t, clk = %h, out = %h", $time, clk, out);
  end

  initial begin
      $dumpfile("test.vcd"); // the name of the file to write the dumped vars to
      $dumplimit(65536);     // the limit in bytes of the file size
      $dumpvars(0, test);    // which modules' vars to dump
      # 10 rst = 0;
      # 100 $finish;
  end
 
endmodule // test
