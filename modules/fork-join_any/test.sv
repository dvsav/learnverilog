// `timescale <time_unit>/<time_precision>
`timescale 10ns/1ns

module test;

initial begin
    fork
        // Thread 1
        begin
            repeat(10) begin
                #1 $display("Thread 1");
            end
        end
        
        // Thread 2
        begin
            repeat(10) begin
                #2 $display("Thread 2");
            end
        end
        
        // Thread 3
        begin
            repeat(10) begin
                #5 $display("Thread 3");
            end
        end
    join_any
    
    $display("*** One of the threads has finished ***");
end

endmodule
