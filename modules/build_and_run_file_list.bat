@echo off

Rem Extract the name of the current working directory
set working_dir=%~dp0
for /F "delims==/\" %%A in ("%working_dir%") do set foldername=%%~nA

Rem Text file listing Verilog source file names (1st command line argument)
set file_list=%1

Rem Output file name = current working directory + ".vvp"
set out=%foldername%.vvp

Rem Verilog compiler (-g2012 switch is used to support IEEE1800-2012 language standard)
set COMPILE=iverilog.exe -g2012

Rem Simulation runtime engine
set SIMULATE=vvp.exe

Rem Utility for showing waveforms
set WAVEFORM=gtkwave.exe
set waveform_file=*.vcd

echo Compiling...
%COMPILE% -o %out% -c %file_list%

Rem Simulate the design if there are no errors
IF %ERRORLEVEL% EQU 0 (
  echo Simulating...
  %SIMULATE% %out%
  
  IF %ERRORLEVEL% EQU 0 (
    echo Waveform...
    %WAVEFORM% %waveform_file%
  )
)

pause
