// `timescale <time_unit>/<time_precision>
`timescale 10ns/1ns

module test;
    logic cin = 0;
    logic a = 0;
    logic b = 0;
    logic out;
    logic cout;
    
    full_adder add(.cin(cin), .a(a), .b(b), .out(out), .cout(cout));

    initial begin
        cin = 0; a = 0; b = 0; #10;
        assert(out === 0 && cout === 0) else $error("000 failed");
        
        cin = 0; a = 0; b = 1; #10;
        assert(out === 1 && cout === 0) else $error("001 failed");
        
        cin = 0; a = 1; b = 0; #10;
        assert(out === 1 && cout === 0) else $error("010 failed");        
        
        cin = 0; a = 1; b = 1; #10;
        assert(out === 0 && cout === 1) else $error("011 failed");        
        
        cin = 1; a = 0; b = 0; #10;
        assert(out === 1 && cout === 0) else $error("100 failed");        
        
        cin = 1; a = 0; b = 1; #10;
        assert(out === 0 && cout === 1) else $error("101 failed");        
        
        cin = 1; a = 1; b = 0; #10;
        assert(out === 0 && cout === 1) else $error("110 failed");        
        
        cin = 1; a = 1; b = 1; #10;
        assert(out === 1 && cout === 1) else $error("111 failed");        
        
        $finish;
    end
    
    initial begin
        $monitor("At time %t, cin=%h, a=%h, b=%h, out=%h, cout=%h",
                 $time, cin, a, b, out, cout);
    end
              
    initial begin
        $dumpfile("test.vcd"); // the name of the file to write the dumped vars to
        $dumplimit(65536);     // the limit in bytes of the file size
        $dumpvars(0, test);    // which modules' vars to dump
    end
endmodule // test
