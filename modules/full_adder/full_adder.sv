module full_adder(
    input cin, a, b,
    output out, cout);
    
    wire half_out;
    wire half_c;
    wire c;
    
    half_adder hadd1(.a(a), .b(b), .out(half_out), .c(half_c));
    half_adder hadd2(.a(half_out), .b(cin), .out(out), .c(c));

    assign cout = half_c | c;
endmodule
