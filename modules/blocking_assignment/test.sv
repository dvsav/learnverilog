module test;

initial
  begin
    bit[7:0] a, b, x, y;
    
    a = 10;
    b = 15;
    
    x = a + b;
    y = a + b + x;
    
    $display("a = %0d b = %0d x = %0d y = %0d", a, b, x, y);
    $finish;
  end

endmodule
