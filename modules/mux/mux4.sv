module mux4 #(parameter WIDTH = 8)
  (input logic [WIDTH-1:0] d0, d1, d2, d3,
   input logic [1:0] select,
   output logic [WIDTH-1:0] out);
   
   logic [WIDTH-1:0] low, high;
   
   mux2 #(WIDTH) highmux(.d0(d0),  .d1(d1),   .select(select[0]), .out(low));
   mux2 #(WIDTH)  lowmux(.d0(d2),  .d1(d3),   .select(select[0]), .out(high));
   mux2 #(WIDTH)  outmux(.d0(low), .d1(high), .select(select[1]), .out(out));
endmodule
