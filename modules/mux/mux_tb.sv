// `timescale <time_unit>/<time_precision>
`timescale 10ns/1ns

module test;
    `define WIDTH 1

    logic[`WIDTH-1:0] d0 = 0;
    logic[`WIDTH-1:0] d1 = 1;
    logic[`WIDTH-1:0] d2 = 1;
    logic[`WIDTH-1:0] d3 = 0;
    
    logic [1:0] select = 2'b00;
    
    logic [`WIDTH-1:0] out;
    
    mux4 #(`WIDTH) multiplexer(d0, d1, d2, d3, select, out);

    initial begin
        # 10 select = 2'b01;
        # 10 select = 2'b10;
        # 10 select = 2'b11;
        # 10 $finish;
    end
    
    initial begin
        $monitor("At time %t, select=%h, out=%b",
                 $time, select, out);
    end
              
    initial begin
        $dumpfile("test.vcd"); // the name of the file to write the dumped vars to
        $dumplimit(65536);     // the limit in bytes of the file size
        $dumpvars(0, test);    // which modules' vars to dump
    end
endmodule // test
