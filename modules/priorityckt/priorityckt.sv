module priorityckt(
    input logic  [3:0] in,
    output logic [3:0] out);
    
    always_comb
      begin
        if      (in[3]) out = 4'b1000;
        else if (in[2]) out = 4'b0100;
        else if (in[1]) out = 4'b0010;
        else if (in[0]) out = 4'b0001;
        else            out = 4'b0000;
      end

endmodule
