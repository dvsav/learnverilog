// `timescale <time_unit>/<time_precision>
`timescale 10ns/1ns

module test;
    logic [3:0] in = 4'b0000;
    wire  [3:0] out;
    
    priorityckt prio(.in(in), .out(out));

    initial begin
        # 10 in = 4'b1000;
        # 10 in = 4'b1100;
        # 10 in = 4'b1110;
        # 10 in = 4'b1111;
        # 10 in = 4'b0100;
        # 10 in = 4'b0110;
        # 10 in = 4'b0111;
        # 10 in = 4'b0010;
        # 10 in = 4'b0011;
        # 10 in = 4'b0001;
        # 10 $finish;
    end
    
    initial begin
        $monitor("At time %t, in=%h, out=%h",
                 $time, in, out);
    end
              
    initial begin
        $dumpfile("test.vcd"); // the name of the file to write the dumped vars to
        $dumplimit(65536);     // the limit in bytes of the file size
        $dumpvars(0, test);    // which modules' vars to dump
    end
endmodule // test
