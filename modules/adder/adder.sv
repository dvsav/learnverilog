module adder #(parameter WIDTH = 8)
  (input logic [WIDTH-1:0] a, b,
   input logic cin,
   output logic [WIDTH-1:0] s,
   output logic cout);
    
  assign {cout, s} = a + b + cin;
endmodule
