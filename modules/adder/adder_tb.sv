// `timescale <time_unit>/<time_precision>
`timescale 10ns/1ns

module test;
    `define WIDTH 10
    
    // Using SystemVerilog data types:    // Alternatively:
    int unsigned a = 0, b = 0;            // logic[`WIDTH-1:0] a = 0, b = 0;
    bit cin = 0;                          // logic cin = 0;
    logic[`WIDTH-1:0] s;
    logic cout;
    
    adder #(.WIDTH(`WIDTH)) add(a, b, cin, s, cout);

    initial begin
        #10 a = 130; b = 140; cin = 0;
        #10 a = 130; b = 140; cin = 1;
        #10 a = 5;   b = 7;   cin = 0;
        #10 a = 5;   b = 7;   cin = 1;
        #10 $finish;
    end
    
    initial begin
        $monitor("At time %t, a=%d, b=%d, s=%d, cout=%b",
                 $time, a, b, s, cout);
    end
              
    initial begin
        $dumpfile("test.vcd"); // the name of the file to write the dumped vars to
        $dumplimit(65536);     // the limit in bytes of the file size
        $dumpvars(0, test);    // which modules' vars to dump
    end
endmodule // test
