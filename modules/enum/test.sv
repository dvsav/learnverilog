module test;

    enum { red, green, blue, yellow, white, black } color;
    
    initial begin
        color = color.first();
        
        while (color != color.last()) begin
            $display("Value = %0d, Name = %s", color, color.name());
            color = color.next;
        end
        $display("Value = %0d, Name = %s", color, color.name());
        
        $finish;
    end
    
endmodule
