module freq_divide_by3(
    input logic clk,
    input logic reset,
    output logic out);
    
    // defining the states of the Finite State Machine
    typedef enum logic [1:0] {S0, S1, S2} statetype;
    statetype state;
    statetype nextstate;
    
    // sequential part of the module
    always_ff @(posedge clk or posedge reset)
    begin
      if (reset) state <= S0;
      else state <= nextstate;
    end
    
    // combinational part of the logic
    always_comb
    begin
      case (state)
        S0: nextstate = S1;
        S1: nextstate = S2;
        S2: nextstate = S0;
        default: nextstate = S0;
      endcase
    end
    
    assign out = (state == S0);
endmodule
