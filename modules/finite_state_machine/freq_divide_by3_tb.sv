// `timescale <time_unit>/<time_precision>
`timescale 10ns/1ns

module test;
    logic clk = 0;
    logic reset = 0;
    wire out;
    
    freq_divide_by3 fsm(.clk(clk), .reset(reset), .out(out));

    initial begin
        # 10 clk = 0;   $display(fsm.state.name);
        # 10 clk = 1;   $display(fsm.state.name);
        # 10 clk = 0;   $display(fsm.state.name);
        # 10 clk = 'bx; $display(fsm.state.name);
        # 10 clk = 0;   $display(fsm.state.name);
        # 10 clk = 1;   $display(fsm.state.name);
        # 10 clk = 'bz; $display(fsm.state.name);
        # 10 clk = 1;   $display(fsm.state.name);
        # 10 clk = 0;   $display(fsm.state.name);
        # 10 clk = 1;   $display(fsm.state.name);
        # 10 clk = 0;   $display(fsm.state.name);
        # 10 clk = 1;   $display(fsm.state.name);
        # 10 clk = 0;   $display(fsm.state.name);
        # 10 $finish;
    end
    
    initial begin    
        $monitor("At time %t, fsm.state=%d, in=%b, out=%b",
                 $time, fsm.state, clk, out);
    end
              
    initial begin
        $dumpfile("test.vcd"); // the name of the file to write the dumped vars to
        $dumplimit(65536);     // the limit in bytes of the file size
        $dumpvars(0, test);    // which modules' vars to dump
    end
endmodule // test
