module test;

    bit[2:0][7:0] values_packed;
    
    bit[7:0] values_unpacked[2:0];
    
    bit[7:0] values_dynamic[];
    
    initial begin
        values_packed = { 8'd2, 8'd1, 8'd0 };
        
        values_unpacked[0] = 'd0;
        values_unpacked[1] = 'd1;
        values_unpacked[2] = 'd2;
        
        values_dynamic = new[3];
        values_dynamic = '{ 8'd0, 8'd1, 8'd2 };
        
        for (int i = 0; i < 3; i++)
            $display("values_packed[%0d] = %0d", i, values_packed[i]);
            
        for (int i = 0; i < 3; i++)
            $display("values_unpacked[%0d] = %0d", i, values_unpacked[i]);
            
        for (int i = 0; i < values_dynamic.size(); i++)
            $display("values_dynamic[%0d] = %0d", i, values_dynamic[i]);
            
        values_dynamic.delete();
                
        $finish;
    end
    
endmodule
