class Rational;
    // instance properties
    local int numerator;
    local int denominator;
    
    // constructor
    // (SystemVerilog doesn't support function overload,
    // so there can be only one constructor)
    function new();
        this.numerator = 0;
        this.denominator = 1;
    endfunction
            
    // method
    task set(int _numerator, int _denominator);
        this.numerator = _numerator;
        this.denominator = _denominator;
    endtask
    
    // property setter
    task setNumerator(int value);
        numerator = value;
    endtask
    
    // property getter
    function int getNumerator();
        return numerator;
    endfunction
        
    // property setter
    task setDenominator(int value);
        denominator = value;
    endtask
        
    // property getter
    function int getDenominator();
        return denominator;
    endfunction

    // method
    task Display();
        $display("%0d / %0d", numerator, denominator);
    endtask
endclass

class parent_class;
    protected bit [31:0] addr;
endclass
 
class child_class extends parent_class;
    local bit [31:0] data;
    
    task Display();
        $display("addr = %0d data = %0d", addr, data);
    endtask
endclass

module test;

    Rational r;
    initial begin
        r = new();
        r.set(1, 2);
        r.Display();
        
        // $display("%0d / %0d", r.numerator, r.denominator); // ERROR! Local properties aren't accessible outside of the class.
        $display("%0d / %0d", r.getNumerator(), r.getDenominator());
    end
    
    child_class c;
    initial begin
        c = new();
        c.Display();
        
        // $display("addr = %0d", c.addr); // ERROR! Protected properties aren't accessible outside of the class.
    end

endmodule
