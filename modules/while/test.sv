module test;

initial
    begin
        int i;
        
        // while
        while (i < 10) begin
            $display("i = %0d", i);
            i++;
        end
        
        // do-while
        i = 0;
        do begin
            $display("i = %0d", i);
            i++;
        end
        while (i < 10);
    
        $finish;
    end

endmodule
