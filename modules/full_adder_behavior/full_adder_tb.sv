// `timescale <time_unit>/<time_precision>
`timescale 10ns/1ns

module test;
    logic cin = 0;
    logic a = 0;
    logic b = 0;
    wire out;
    wire cout;
    
    full_adder add(.cin(cin), .a(a), .b(b), .out(out), .cout(cout));

    initial begin
        # 10  cin = 0; a = 0; b = 0;
        # 20  cin = 0; a = 0; b = 1;
        # 30  cin = 0; a = 1; b = 0;
        # 40  cin = 0; a = 1; b = 1;
        # 50  cin = 1; a = 0; b = 0;
        # 60  cin = 1; a = 0; b = 1;
        # 70  cin = 1; a = 1; b = 0;
        # 80  cin = 1; a = 1; b = 1;
        # 100 $finish;
    end
    
    initial begin
        $monitor("At time %t, cin=%h, a=%h, b=%h, out=%h, cout=%h",
                 $time, cin, a, b, out, cout);
    end
              
    initial begin
        $dumpfile("test.vcd"); // the name of the file to write the dumped vars to
        $dumplimit(65536);     // the limit in bytes of the file size
        $dumpvars(0, test);    // which modules' vars to dump
    end
endmodule // test
