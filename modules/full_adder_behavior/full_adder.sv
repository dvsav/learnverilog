module full_adder(input logic cin, a, b,
                  output logic out, cout);

    logic p, g;
    
    always_comb
      begin
        p = a ^ b;
        g = a & b;
        out = p ^ cin;
        cout = g | (p & cin);
      end

endmodule
