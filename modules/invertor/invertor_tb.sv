// `timescale <time_unit>/<time_precision>
`timescale 10ns/1ns

module test;
    logic[3:0] in = 0;
    wire[3:0] out;
    
    invertor inv(.in(in), .out(out));

    initial begin
        # 10  in = 4'b0001;
        # 20  in = 4'b0010;
        # 30  in = 4'b0100;
        # 40  in = 4'b1000;
        # 50 $finish;
    end
    
    initial begin
        $monitor("At time %t, in=%h, out=%h",
                 $time, in, out);
    end
              
    initial begin
        $dumpfile("test.vcd"); // the name of the file to write the dumped vars to
        $dumplimit(65536);     // the limit in bytes of the file size
        $dumpvars(0, test);    // which modules' vars to dump
    end
endmodule // test
