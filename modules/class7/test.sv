class Rational;
    // instance properties
    local int numerator;
    local int denominator;
    
    // constructor
    // (SystemVerilog doesn't support function overload,
    // so there can be only one constructor)
    function new();
        this.numerator = 0;
        this.denominator = 1;
        nInstances++;
    endfunction
        
    // method
    task set(int _numerator, int _denominator);
        this.numerator = _numerator;
        this.denominator = _denominator;
    endtask

    // method
    extern task Display();
endclass

task Rational::Display();
    $display("%0d / %0d", numerator, denominator);
endtask

module test;

    Rational r;
    initial begin
        r = new();
        r.set(1, 2);
        r.Display();
    end

endmodule
