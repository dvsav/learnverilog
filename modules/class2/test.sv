// parameterized class with two integer template parameters
class packet #(parameter int ADDR_WIDTH = 32, DATA_WIDTH = 32);
    bit [ADDR_WIDTH-1:0] address;
    bit [DATA_WIDTH-1:0] data;
 
    function new();
        address = 10;
        data = 20;
    endfunction
endclass

class packetT #(parameter type TA = int, type TD = int);
    TA address;
    TD data;
 
    function new();
        address = 10;
        data = 20;
    endfunction
endclass

module test;
    packet #(.ADDR_WIDTH(16), .DATA_WIDTH(20)) pack;
    packetT #(.TA(shortint), .TD(int)) packT;
    
    initial begin
        pack = new();
        $display("address = %0d, data = %0d", pack.address, pack.data);
        
        packT = new();
        $display("address = %0d, data = %0d", packT.address, packT.data);
    end

endmodule
