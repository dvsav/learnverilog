// `timescale <time_unit>/<time_precision>
`timescale 10ns/1ns

module test;

  /* Make a reset that pulses once. */
  logic reset = 0;
  initial begin
      # 17 reset = 1;
      # 10 reset = 0;
      # 29 reset = 1;
      # 10 reset = 0;
      # 100 $finish;
  end

  /* Make a regular pulsing clock. */
  logic clk = 0;
  always #5 clk = !clk;

  logic [7:0] value;
  counter #(8) c1 (.out(value), .clk(clk), .reset(reset));

  initial
     $monitor("At time %t, value = %h (%0d)",
              $time, value, value);
              
  initial begin
      $dumpfile("test.vcd"); // the name of the file to write the dumped vars to
      $dumplimit(65536);     // the limit in bytes of the file size
      $dumpvars(0, test);    // which modules' vars to dump
  end
 
endmodule // test
