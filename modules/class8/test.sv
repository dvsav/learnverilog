typedef class classB; // class forward declaration

class classA;
    classB cb; //using classB handle before declaring it.
endclass
 
class classB;
    classA ca;
endclass
  
module typedef_class;
    classA a;
    classB b;
    initial begin
        $display("Hello");
    end
endmodule