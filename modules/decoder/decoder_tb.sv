// `timescale <time_unit>/<time_precision>
`timescale 10ns/1ns

module test;
    logic [2:0] in = 3'b000;
    wire [7:0] out;
    
    decoder #(3) decoder(.in(in), .out(out));
    // decoder3_8 decoder(.in(in), .out(out));

    initial begin
        # 10 in = 1;
        # 10 in = 2;
        # 10 in = 3;
        # 10 in = 4;
        # 10 in = 5;
        # 10 in = 6;
        # 10 in = 7;
        # 10 $finish;
    end
    
    initial begin
        $monitor("At time %t, in=%h, out=%h",
                 $time, in, out);
    end
              
    initial begin
        $dumpfile("test.vcd"); // the name of the file to write the dumped vars to
        $dumplimit(65536);     // the limit in bytes of the file size
        $dumpvars(0, test);    // which modules' vars to dump
    end
endmodule // test
