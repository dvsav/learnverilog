//class
class packet;
    rand  bit [2:0] addr1;
    
    // randc is random-cyclic. For the variables declared with the randc keyword, on randomization
    // variable values don’t repeat a random value until every possible value has been assigned.
    randc bit [2:0] addr2; 
endclass
 
module rand_methods;
    packet pkt;
    
    initial begin
        pkt = new();
        
        repeat(10) begin
            pkt.randomize();
            $display("addr1 = %0d addr2 = %0d", pkt.addr1, pkt.addr2);
        end
    end
endmodule