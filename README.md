# LearnVerilog

LearnVerilog is an amateur Verilog / SystemVerilog project created for educational purposes.  

## Prerequisites

To build hardware designs from the LearnVerilog you will need the following software:  
[Windows 7 or newer](https://www.microsoft.com/windows/) (required)  
[Icarus Verilog for Windows](http://bleyer.org/icarus/) (required)
[GTKWave Electronic Waveform Viewer](http://www.dspia.com/gtkwave.html) (required)

## Getting Started

Launch git command line interface. In the command prompt go to the folder to which you want to copy the LearnVerilog source code.  
Type: `git clone https://bitbucket.org/dvsav/learnverilog.git` to download the source code on your computer.  

In the [modules] folder you'll find other folders each one containing a hardware design project. For example [modules/hello] contains the simple test example which prints "Hello, World!" string on the screen. To build a project launch the [build_and_run.bat] file from that project's folder (for example, by double clicking on that bat-file in Windows Explorer).

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
